// selects form elements
const form = document.getElementById('form');
const errorElement = document.getElementById('error');

// displays error messages
form.addEventListener('submit', (e) => {
    let messages = []

    // checks for name errors
    function nameError(){
        const firstName = document.getElementById('firstname');
        const lastName = document.getElementById('lastname');
        const letters = /^[A-Za-z]+$/;
        if (firstName.value.match(letters)) {
            return true;
        }
        else {
            messages.push("! Your first name must contain letters only.");
        }
        if (lastName.value.match(letters)) {
            return true;
        }
        else {
            messages.push("! Your last name must contain letters only.");
        }
    }
    nameError()

     // checks for age errors
    function ageError(){
        var birthDate = new Date(document.getElementById('birthdate').value);
        var difference = new Date(Date.now() - birthDate.getTime());
        var age = Math.abs(difference.getUTCFullYear() - 1970);

        if (age < 13) {
            messages.push("! Users under 13 are prohibited from registering.");
        }

        if (age > 50) {
            alert("If you require assistance, please visit https://fakelink.aptechrepairs/help/");
        }
    }
    ageError()

    // checks for email errors
    function emailError(){
        const email = document.getElementById('email');
        const emailValidate = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(email);
        if (emailValidate == false) {
            messages.push("! Your email is invalid. Please, check the spelling.");
        }
    }

    // checks for password errors
    function passwordError(){
        const password = document.getElementById('password');
        if (password.value.length < 8) {
            messages.push("! Your password cannot be less than 8 characters.");
        }
        
        else if (password.value.length > 16) {
            messages.push("! Your password cannot be more than 16 characters.");
        }
    }
    passwordError()

    // prevents form from submitting
    if (messages.length > 0) {
        e.preventDefault();
        errorElement.innerText = messages.join('\n');
    }
})