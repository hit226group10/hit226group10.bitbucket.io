//variables
const mainForm = document.getElementById("mainForm");

/*** Name Input Validation ***/ 

//event listener when user highlights the input box or enters something
const fname = document.getElementById('fname');
fname.addEventListener("input", showNameEmptyError);
fname.addEventListener("focus", showNameEmptyError); 

//function that shows a warning if there are no inputs
function showNameEmptyError(){
    const fnameInput = document.getElementById('fname').value;
    const fnameError = document.getElementById('fnameError');
    if(fnameInput.length < 1){
        fnameError.className = "errorMsg visible"; //makes the div element with nameError ID visible showing the error message if there is no input
        fname.className = "form-control inputBorderError"; //add class inputBorderError to change border color to red on invalid input
    } else {
        fnameError.className = "errorMsg invisible"; //removes the error message if there is an input
        fname.className = "form-control inputBorderSuccess"; //add class inputBorderSuccess to change border color to yellow on valid input
    }  
}

/*** Address Input Validation ***/ 

//event listener when user highlights the input box or enters something
const address = document.getElementById('address');
address.addEventListener("input", showAddressEmptyError);
address.addEventListener("focus", showAddressEmptyError);

//function that shows a warning if there are no inputs
function showAddressEmptyError(){
    const addressInput = document.getElementById('address').value;
    const addressError = document.getElementById('addressError');
    if(addressInput.length < 1){
        addressError.className = "errorMsg visible"; 
        address.className = "form-control inputBorderError"; 
    } else {
        addressError.className = "errorMsg invisible";
        address.className = "form-control inputBorderSuccess"; 
    }  
}

/*** Email Input Validation ***/ 

//event listener when user highlights the input box or enters something
const email = document.getElementById('email');
email.addEventListener("input", showEmailEmptyError);
email.addEventListener("focus", showEmailEmptyError);
email.addEventListener("input", emailPatternValidation);
mainForm.addEventListener("submit", emailPatternValidation); //prevents form from submitting if email is invalid

//function that shows a warning if there are no inputs
function showEmailEmptyError(){
    const emailInput = document.getElementById('email').value;
    const emailError = document.getElementById('emailError');
    if(emailInput.length < 1){
        emailError.className = "errorMsg visible";
        email.className = "form-control inputBorderError"; 
    } 
}

//function that checks if user's email address input is correct using Regular Expression 
function emailPatternValidation(event){
    const emailInput = document.getElementById('email').value;
    const emailError = document.getElementById('emailError');
    const emailCheck = /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(emailInput); //regex pattern for email validation
    if(emailCheck === false){
        emailError.className = "errorMsg visible";
        email.className = "form-control inputBorderError"; 
        event.preventDefault(); //prevents form from submitting if email is invalid
    } else {
        emailError.className = "errorMsg invisible";
        email.className = "form-control inputBorderSuccess"; 
    }  
}


/*** Password Input Validation ***/ 

//event listener when user highlights the input box or enters something
const password = document.getElementById('password');
password.addEventListener("input", showPasswordEmptyError);
password.addEventListener("focus", showPasswordEmptyError);

//function that shows a warning if there are no inputs
function showPasswordEmptyError(){
    const passwordInput = document.getElementById('password').value;
    const passwordError = document.getElementById('passwordError');
    if(passwordInput.length < 1){
        passwordError.className = "errorMsg visible";
        password.className = "form-control inputBorderError"; 
    } else {
        passwordError.className = "errorMsg invisible";
        password.className = "form-control inputBorderSuccess"; 
    }  
}

/*** Telephone Input Validation ***/                               

//event listener when user highlights the input box or enters something
const telphone = document.getElementById('telphone');
telphone.addEventListener("input", showTelphoneEmptyError);
telphone.addEventListener("focus", showTelphoneEmptyError);

//function that shows a warning if there are no inputs
function showTelphoneEmptyError(){
    const telphoneInput = document.getElementById('telphone').value;
    const telphoneError = document.getElementById('telphoneError');
    if(telphoneInput.length < 1){
        telphoneError.className = "errorMsg visible";
        telphone.className = "form-control inputBorderError"; 
    } else {
        telphoneError.className = "errorMsg invisible";
        telphone.className = "form-control inputBorderSuccess"; 
    }  
}

/*** Birthdate Input Validation ***/ 

//event listener when user highlights the input box or enters something or submits the form
const bdate = document.getElementById('bdate');
bdate.addEventListener("input", showBdateEmptyError);
bdate.addEventListener("focus", showBdateEmptyError);
bdate.addEventListener("input", showBdateAgeMsg);
mainForm.addEventListener("submit", showBdateAgeMsg); // prevents user from submitting form if age is below 13

//function that shows a warning if there are no inputs
function showBdateEmptyError(){
    const bdateInput = document.getElementById('bdate').value;
    const bdateError = document.getElementById('bdateError');
    if(bdateInput.length < 1){
        bdateError.className = "errorMsg visible";
        bdate.className = "form-control inputBorderError"; 
    } else {
        bdateError.className = "errorMsg invisible";
        bdate.className = "form-control inputBorderSuccess"; 
    }
}

//function that shows warning and stop user from submitting the form if age < 13 and help if age > 50. 
function showBdateAgeMsg(event){
    const bdateWarning = document.getElementById('bdateWarning');
    const bdateHelp = document.getElementById('bdateHelp');
    const bdateInput = document.getElementById('bdate').value;
    const birthDate = new Date(bdateInput); //get user birthdate
    const dateDifferenceMs = Date.now() - birthDate.getTime(); //difference in milliseconds
    const dateDifference = new Date(dateDifferenceMs); //shows difference in date format with default starting on Jan 1 1970
    const userAge = Math.abs(dateDifference.getUTCFullYear() - 1970); //shows difference in years which shows user's age

    if (userAge < 13){
        bdateWarning.className = "warningMsg visible";
        bdateHelp.className = "helpMsg invisible";
        bdate.className = "form-control inputBorderError"; 
        event.preventDefault(); // prevents user from submitting form if age is below 13
    } else if (userAge > 50) {
        bdateWarning.className = "warningMsg invisible";
        bdateHelp.className = "helpMsg visible";
        bdateError.className = "errorMsg invisible";
        bdate.className = "form-control inputBorderSuccess"; 
    } else {
        bdateWarning.className = "warningMsg invisible";
        bdateHelp.className = "helpMsg invisible";
        bdateError.className = "errorMsg invisible";
        bdate.className = "form-control inputBorderSuccess"; 
    }
}

/*** Email Autocomplete Function ***/ 

//Function that provides autocomplete to the Address input using Twitter's Typeahead plugin from Github
$(document).ready(function(){
    // Defining the local dataset of suggestions
    var addressAutoComplete = [
    'Abafar',
    'Agamar',
    'Ahch-To',
    'Ajan Kloss',
    'Akiva',
    'Alderaan',
    'Aleen',
    'Alzoc III',
    'Anaxes',
    'Ando',
    'Anoat',
    'Atollon',
    'Balnab',
    'Batuu',
    'Bespin',
    'Bracca',
    'Cantonica',
    'Castilon',
    'Cato Neimoidia',
    'Chandrila',
    'Christophsis',
    'Concord Dawn',
    'Corellia',
    'Coruscant',
    'Crait',
    'DQar',
    'Dagobah',
    'Dantooine',
    'Dathomir',
    'Devaron',
    'Eadu',
    'Endor',
    'Endor (Sanctuary)',
    'Er´kit',
    'Eriadu',
    'Esseles',
    'Exegol',
    'Felucia',
    'Florrum',
    'Fondor',
    'Geonosis',
    'Hosnian Prime',
    'Hoth',
    'Iego',
    'Ilum',
    'Iridonia',
    'Jakku',
    'Jedha',
    'Jestefad',
    'Kamino',
    'Kashyyyk',
    'Kef Bir',
    'Kessel',
    'Kijimi',
    'Kuat',
    'Lahmu',
    'Lothal',
    'Lotho Minor',
    'Malachor',
    'Malastare',
    'Mandalore',
    'Maridun',
    'Mimban',
    'Mon Cala',
    'Moraband',
    'Mortis',
    'Mustafar',
    'Mygeeto',
    'Naboo',
    'Nal Hutta',
    'Nevarro',
    'Numidian Prime',
    'Onderon',
    'Ord Mantell',
    'Pasaana',
    'Pillio',
    'Polis Massa',
    'Rishi',
    'Rodia',
    'Rugosa',
    'Ruusan',
    'Ryloth',
    'Saleucami',
    'Savareen',
    'Scarif',
    'Serenno',
    'Shili',
    'Sissubo',
    'Skako Minor',
    'Sorgan',
    'Starkiller Base',
    'Subterrel',
    'Sullust',
    'Takodana',
    'Tatooine',
    'Teth',
    'Toydaria',
    'Trandosha',
    'Umbara',
    'Utapau',
    'Vandor-1',
    'Vardos',
    'Wobani',
    'Wrea',
    'Yavin',
    'Yavin 4',
    'Zeffo',
    'Zygerria',
    ];
    
    // Constructing the suggestion engine
    var addressAutoComplete = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        local: addressAutoComplete
    });
    
    // Initializing the typeahead
    $('.typeahead').typeahead({
        hint: true,
        highlight: true, /* Enable substring highlighting */
        minLength: 1 /* Specify minimum characters required for showing suggestions */
    },
    {
        name: 'addressAutoComplete',
        source: addressAutoComplete
    });
});










