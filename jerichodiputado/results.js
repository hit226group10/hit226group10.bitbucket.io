//function that will generate a random number 
function midiCount(){
    const midi = document.getElementById("midi");
    const judgement = document.getElementById("judgement");
    const randomNumber = Math.floor((Math.random() * 25000) + 1); //generate a random number from 1 to 25,000
    midi.innerHTML = "Midi-chlorian count: " + randomNumber;
    
    if (randomNumber <= 100){  
        judgement.innerHTML = "You are a Galactic POTATO!!!"
    } else if (randomNumber > 100 && randomNumber <= 2500){
        judgement.innerHTML = "You are a Galactic Commoner."
    } else if (randomNumber > 2500 && randomNumber <= 7500){
        judgement.innerHTML = "You are a Galactic Wizard."
    } else if (randomNumber > 7500 && randomNumber <= 15000){
        judgement.innerHTML = "You are a STRONG Galactic Wizard!"
    } else if (randomNumber > 15000 && randomNumber <= 20000){
        judgement.innerHTML = "You are a VERY STRONG Galactic Wizard!!"
    } else if (randomNumber > 20000 && randomNumber <= 25000){
        judgement.innerHTML = "You are an OVERPOWERED Galactic Wizard!!!"
    }
}
    